const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.
output=[]
function keys(obj) {
  for(const key in obj){
    output.push(key);
  }
  return output;
}
output2=[]
function values(obj) {
  for(const key in obj){
    output2.push(obj[key]);
  }
  return output2;
}
output3={}
function mapObject(obj, cb) {
  for(const key in obj){
    output3[key]=cb(obj[key],key);
  }
  return output3;
}
output4=[]
function pairs(obj) {
  for(const key in obj){
    temp=[key,obj[key]];
    output.push(temp);
  }
  return output4;
  
}

/* STRETCH PROBLEMS */
output5={}
function invert(obj) {
  for(const key in obj){
    output5[obj[key]]=key;
  }
  return output5;
}
function defaults(obj, defaultProps) {
  for(const key in defaultProps){
    obj[key]=defaultProps[key]
  }
  return obj;
}
