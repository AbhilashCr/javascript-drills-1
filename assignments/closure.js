function counterFactory() {
  // Return an object that has two methods called `increment` and `decrement`.
  // `increment` should increment a counter variable in closure scope and return it.
  // `decrement` should decrement the counter variable and return it.
  let counter=0;
  let addone = () => counter++;
  let subone = () => counter--;
  return {
    increment () {
         return addone();
    },
    decrement () {
         return subone();
    }
  }
}

function limitFunctionCallCount(cb, n) {
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
  let func2 = (cb,n) => {
        if(typeof(cb)==='function'){
            for(let i=0;i<n;i++){
               cb();
            }
        }
        else return 'null';
  }
  return func2(cb,n);
}

function cacheFunction(cb) {
  // Should return a funciton that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invoke this function.
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.
  let cache={};
  let func2 = (n) =>{
         if(n in cache) {
            return cache[n]
         }
         else{
            let a=cb();
            cache[n]=a;
            return a;
          }
       }
       return func2(n)
  
}
